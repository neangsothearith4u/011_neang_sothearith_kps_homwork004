import React, { Component } from "react";
import Swal from "sweetalert2";
import "animate.css";
export default class Table extends Component {
  // show details information by ID using SweetAlert
  handleSelectId = (item) => {
    Swal.fire({
      html: `ID : ${item.id} <br/> Email : ${item.email} <br/> Username : ${item.username} <br/> Age : ${item.age} <br/>`,
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
  };

  render() {
    return (
      <div className="w-4/5 justify-center m-auto">
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table class="w-full text-sm text-left ">
            <thead class="text-xs text-gray-700 uppercase backdrop-blur-sm bg-white/30 ">
              <tr>
                <th scope="col" class="px-6 py-3">
                  id
                </th>
                <th scope="col" class="px-6 py-3">
                  email
                </th>
                <th scope="col" class="px-6 py-3">
                  username
                </th>
                <th scope="col" class="px-6 py-3">
                  age
                </th>
                <th scope="col" class="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr
                  class={`border-b ${
                    item.id % 2 == 0
                      ? "bg-fuchsia-100"
                      : "backdrop-blur-sm bg-white/70"
                  }`}
                  key={item.id}
                >
                  <th
                    scope="row"
                    class="px-6 py-4 font-medium  whitespace-nowrap text-blue-600 "
                  >
                    {item.id}
                  </th>
                  <td class="px-6 py-4">
                    {item.email == "" ? "Null" : item.email}
                  </td>
                  <td class="px-6 py-4">
                    {item.username == "" ? "Null" : item.username}
                  </td>
                  <td class="px-6 py-4">
                    {item.age == "" ? "Null" : item.age}
                  </td>
                  <td>
                    {/* Button Pending */}
                    <button
                      onClick={() => this.props.id(item.id)}
                      class={`rounded-xl border-b-4 border-blue-700 hover:border-blue-500 roundedhover:bg-blue-700 text-white font-bold py-2 px-2 -ml-10 mr-5  ${
                        item.status == "Pending" ? "bg-red-400" : "bg-green-400"
                      }`}
                    >
                      {item.status}
                    </button>
                    {/* Button view */}
                    <button
                      onClick={() => this.handleSelectId(item)}
                      class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded-xl -mr-36"
                    >
                      View
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
