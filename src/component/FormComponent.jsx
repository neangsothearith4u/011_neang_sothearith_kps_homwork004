import React, { Component } from "react";
import Table from "./Table";

export default class extends Component {
  constructor() {
    super();
    this.state = {
      userInfo: [
        {
          id: 1,
          email: "thearith@gmail.com",
          username: "thearith",
          age: 99,
          status: "Pending",
        },
        {
          id: 2,
          email: "sreyleak@gmail.com",
          username: "sreyleak",
          age: 99,
          status: "Pending",
        },
        {
          id: 3,
          email: "theary@gmail.com",
          username: "theary",
          age: 99,
          status: "Pending",
        },
      ],
      newEmail: "",
      newUsername: "",
      newAge: "",
      newStatus: "Pending",
    };
  }
  // get value from Email
  handleChangeNewEmail = (e) => {
    this.setState({
      newEmail: e.target.value,
    });
    // console.log("Email :",e.target.value);
  };
  // get value from Username
  handleChangeNewUserName = (e) => {
    this.setState({
      newUsername: e.target.value,
    });
    // console.log("username : ",e.target.value);
  };
  // get value from Age
  handleChangeNewAge = (e) => {
    this.setState({
      newAge: e.target.value,
    });
    // console.log("age : ",e.target.value);
  };
  // submit value to table
  handleSubmit = () => {
    // prepare data to object
    let newObj = {
      id: this.state.userInfo.length + 1,
      email: this.state.newEmail,
      username: this.state.newUsername,
      age: this.state.newAge,
      status: this.state.newStatus,
    };
    // console.log(newObj);

    // set new object to object
    this.setState(
      {
        userInfo: [...this.state.userInfo, newObj],
        newEmail: "",
        newUsername: "",
        newAge: "",
        newStatus: "Pending",
      }
      // () => console.log(this.state.userInfo)
    );
  };

  // Catch value from table component and validate on status change
  getId = (id) => {
    this.state.userInfo.map((item) => {
      if (item.id == id) {
        item.status === "Pending"
          ? (item.status = "Done")
          : (item.status = "Pending");
      }
    });
    this.setState({
      userInfo: this.state.userInfo,
    });
  };

  render() {
    return (
      <div>
        <div>
          <h1 className="font-bold font-link background uppercase">
            Please fill your information
          </h1>
          <div className="flex justify-center">
            <div className="w-2/5 ">
              {/* Email */}
              <label
                for="email-address-icon"
                class="block mb-2 text-lg font-bold text-gray-500  text-left"
              >
                Email
              </label>
              <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none ">
                  <svg
                    aria-hidden="true"
                    class="w-5 h-5 text-gray-500 "
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                    <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                  </svg>
                </div>
                <input
                  type="text"
                  id="email-address-icon"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  "
                  placeholder="example@gmail.com"
                  onChange={this.handleChangeNewEmail}
                  value={this.state.newEmail}
                />
              </div>
              {/* Username  */}
              <label
                for="website-admin"
                class="block mb-2 text-lg font-bold text-gray-500  text-left"
              >
                Username
              </label>
              <div class="flex">
                <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
                  @
                </span>
                <input
                  type="text"
                  id="website-admin"
                  class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  "
                  placeholder="Neang Sothearith"
                  onChange={this.handleChangeNewUserName}
                  value={this.state.newUsername}
                />
              </div>
              {/* Age */}
              <label
                for="website-admin"
                class="block mb-2 text-lg font-bold text-gray-500  text-left"
              >
                Age
              </label>

              <div class="flex">
                <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
                  💗
                </span>
                <input
                  type="text"
                  id="website-admin"
                  class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  "
                  placeholder="22"
                  onChange={this.handleChangeNewAge}
                  value={this.state.newAge}
                />
              </div>
            </div>
          </div>
        </div>
        <div>
          <br />
          <a
            onClick={this.handleSubmit}
            href="#_"
            class="relative inline-flex items-center justify-center px-6 py-3 text-lg font-medium tracking-tighter text-white bg-gray-800 rounded-md group"
          >
            <span class="absolute inset-0 w-full h-full mt-1 ml-1 transition-all duration-300 ease-in-out bg-purple-600 rounded-md group-hover:mt-0 group-hover:ml-0"></span>
            <span class="absolute inset-0 w-full h-full bg-white rounded-md "></span>
            <span class="absolute inset-0 w-full h-full transition-all duration-200 ease-in-out delay-100 bg-purple-600 rounded-md opacity-0 group-hover:opacity-100 "></span>
            <span class="relative text-purple-600 transition-colors duration-200 ease-in-out delay-100 group-hover:text-white">
              Register
            </span>
          </a>
        </div>
        <br />
        {/* Table Component */}
        <div>
          <Table data={this.state.userInfo} id={this.getId}></Table>
        </div>
      </div>
    );
  }
}
